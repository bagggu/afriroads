jQuery(function(o){o(".woocommerce-ordering").on("change","select.orderby",function(){o(this).closest("form").submit()}),o("input.qty:not(.product-quantity input.qty)").each(function(){var e=parseFloat(o(this).attr("min"));e>=0&&parseFloat(o(this).val())<e&&o(this).val(e)}),jQuery(".woocommerce-store-notice__dismiss-link").click(function(){Cookies.set("store_notice","hidden",{path:"/"}),jQuery(".woocommerce-store-notice").hide()}),"hidden"===Cookies.get("store_notice")?jQuery(".woocommerce-store-notice").hide():jQuery(".woocommerce-store-notice").show()});
/*
     FILE ARCHIVED ON 08:50:20 Jun 03, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 23:26:20 Feb 17, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 253.339 (3)
  esindex: 0.01
  captures_list: 272.451
  CDXLines.iter: 9.886 (3)
  PetaboxLoader3.datanode: 297.441 (5)
  exclusion.robots: 0.218
  exclusion.robots.policy: 0.202
  RedisCDXSource: 5.947
  PetaboxLoader3.resolve: 67.6 (3)
  load_resource: 162.145
*/