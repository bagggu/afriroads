jQuery(function(t){var e=t(panelsStyles.fullContainer);0===e.length&&(e=t("body"));var r=function(){t(".siteorigin-panels-stretch.panel-row-style").each(function(){var r=t(this);r.css({"margin-left":0,"margin-right":0,"padding-left":0,"padding-right":0});var i=r.offset().left-e.offset().left,n=e.outerWidth()-i-r.parent().outerWidth();r.css({"margin-left":-i,"margin-right":-n,"padding-left":"full"===r.data("stretch-type")?i:0,"padding-right":"full"===r.data("stretch-type")?n:0});var a=r.find("> .panel-grid-cell");"full-stretched"===r.data("stretch-type")&&1===a.length&&a.css({"padding-left":0,"padding-right":0}),r.css({"border-left":0,"border-right":0})}),t(".siteorigin-panels-stretch.panel-row-style").length&&t(window).trigger("panelsStretchRows")};t(window).resize(r).load(r),r(),t("body").removeClass("siteorigin-panels-before-js")});
/*
     FILE ARCHIVED ON 10:51:04 Jun 03, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 23:26:21 Feb 17, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 482.021 (3)
  esindex: 0.018
  captures_list: 497.823
  CDXLines.iter: 10.792 (3)
  PetaboxLoader3.datanode: 524.184 (5)
  exclusion.robots: 0.436
  exclusion.robots.policy: 0.422
  RedisCDXSource: 1.94
  PetaboxLoader3.resolve: 99.848 (4)
  load_resource: 269.773
*/