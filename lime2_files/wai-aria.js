jQuery( function ( $ ) {
	// Focus styles for menus when using keyboard navigation

	// Properly update the ARIA states on focus (keyboard) and mouse over events
	$( 'nav > ul' ).on( 'focus.wparia  mouseenter.wparia', '[aria-haspopup="true"]', function ( ev ) {
		$( ev.currentTarget ).attr( 'aria-expanded', true );
	} );

	// Properly update the ARIA states on blur (keyboard) and mouse out events
	$( 'nav > ul' ).on( 'blur.wparia  mouseleave.wparia', '[aria-haspopup="true"]', function ( ev ) {
		$( ev.currentTarget ).attr( 'aria-expanded', false );
	} );
} );
/*
     FILE ARCHIVED ON 06:53:36 Jun 03, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 23:26:20 Feb 17, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 95.162 (3)
  esindex: 0.013
  captures_list: 121.141
  CDXLines.iter: 15.254 (3)
  PetaboxLoader3.datanode: 127.558 (5)
  exclusion.robots: 0.19
  exclusion.robots.policy: 0.179
  RedisCDXSource: 6.504
  PetaboxLoader3.resolve: 231.978 (3)
  load_resource: 337.159
*/